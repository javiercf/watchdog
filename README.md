WATCHDOG APP
============

Aplicación que busca la regulación de los proyectos normativos radicados en el Congreso de la República de Colombia por medio de un feed en tiempo real, que contenga un archivo histórico de los proyectos que están siendo radicados en el congreso, quienes votaron y como votaron por el, un dashboard que permita ver estas estadísticas por afiliación política, la búsqueda por palabras clave de proyectos y su estado (Radicado, Publicado, Aprobado, Archivado, Retirado, Acumulado), así como información relevante de congresistas con el fin de facilitar y promover la comunicación entre ciudadanos y representantes con el fin de asegurar que se estén velando por los intereses del público.

Info de Senadores
-----------------
| _id | nombre | apellidos | twitter_handle | partido | comisión | ausencias |
| --- | ------ | --------- | -------------- | ------- | -------- | --------- |
| Identificador único dentro del sistema asignado automaticamente por la plataforma | Nombre de el/la senador/a | Apellidos de el/la senador/a | Usuario de Twitter de el/la senador/a | Afiliación pólitica | Comisión de el/la senador/a | Ausencias en el periodo

Info de Proyectos
-----------------

| _id | nombre | descripción | autor | ponentes | estado | tipo | votaciones |
| --- | ------ | ----------- | ----- | -------- | ------ | ---- | ---------- |
| Identificador único dentro del sistema asignado automaticamente por la plataforma | Nombre del proyecto | descripción del proyecto | Autor del proyecto | Ponentes del Proyecto | Estado del proyecto (Radicado, Publicado, Aprobado, Archivado, Retirado, Acumulado) | tipo de proyecto | Información de las votaciones referentes al proyecto

Votaciones
----------

| _id | _id proyecto | votos |
| --- | ------------ | ----- |
| Identificador único dentro del sistema asignado automaticamente por la plataforma | Identificador único del proyecto | Referencia de Votos

Referencia de Votos
-------------------

**Estos datos se encuentran divididos por comisiones**

| _id senador | si | no | abstinencia | ausencia |
| ----------- | -- | -- | ----------- | -------- |
| senador | true/false | true/false | true/false | true/false |

